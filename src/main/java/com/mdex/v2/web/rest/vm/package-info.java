/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mdex.v2.web.rest.vm;
